package com.example.idaptest;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class Controller implements OnItemClickListener{
	private MyModel m;
	private int position; 
	
	public Controller(MyModel m) {
		this.m = m;

	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		parent.showContextMenuForChild(view);
		this.position = position;
		
	}

	public void addString() {
		m.addData();
		
	}
	
	public void deleteString() {
		m.deleteData(position);
 	}
	
	public void sortString() {
		m.sortData();
	}
	
	public void closeApplication() {
		m.savaData();
	}
	
}
