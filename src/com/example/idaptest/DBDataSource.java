package com.example.idaptest;

import java.util.ArrayList;
import java.util.List;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DBDataSource implements dataCreate {
	private MyDBHelper dbHelper;
	
	
	DBDataSource(MyDBHelper dbHelper) {
		this.dbHelper = dbHelper;
	}

	@Override
	public List<String> getData() {
		
		List<String> arr = new ArrayList<String>();
		
		SQLiteDatabase db=dbHelper.getReadableDatabase();
		Cursor c = db.query(MyDBHelper.tableName, null, null, null, null, null, null);
		if (c.moveToFirst()) {
			int dataColIndex = c.getColumnIndex("data");
	        do {
	        	arr.add(c.getString(dataColIndex));
	          } while (c.moveToNext());
		}
		db.close();
		return arr;
	} 
}
