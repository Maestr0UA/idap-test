package com.example.idaptest;

import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class DataAdapter extends BaseAdapter {
	public List<String> dataList;
	private Activity activity;
	private LayoutInflater inflater=null;
	
	public DataAdapter(Activity a, List<String> dataList) {
		this.dataList = dataList;
		activity = a;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		return dataList.size();
	}

	@Override
	public Object getItem(int position) {
			
		return dataList.get(position);
	}

	@Override
	public long getItemId(int position) {
			
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		TextView dataView;
		View view = convertView;
		if(view == null){
			view = inflater.inflate(R.layout.row, parent,false);
		}	
			
		dataView = (TextView) view.findViewById(R.id.textView1);
		dataView.setText(dataList.get(position));
		
		return view;
	}
}
