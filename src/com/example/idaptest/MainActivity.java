package com.example.idaptest;


import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.app.Activity;


public class MainActivity extends Activity {
	Controller controller;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //this.getApplicationContext().deleteDatabase("IDAP.db");
        MyModel model = new MyModel(this);
        MyView view = new MyView(this.getApplicationContext(), this, model);
        controller = new Controller(model);
        model.addObserver(view);
        setContentView(view);
        
        registerForContextMenu(view);
        view.setOnItemClickListener(controller);
    }

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add(0, 1, 0, "�������� ������");
		menu.add(0, 2, 0, "������� ������");
		menu.add(0, 3, 0, "�����������");
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 1: 
			controller.addString();
			break;
		case 2:
			controller.deleteString();
			break;
		case 3:
			controller.sortString();
			break;
		}
		return super.onContextItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		controller.closeApplication();
		super.onDestroy();
	}
}
