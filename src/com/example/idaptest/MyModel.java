package com.example.idaptest;

import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Random;

import android.app.Activity;
	
public class MyModel extends Observable {
	Activity a;
	private List<String> dataList;
	private MyDBHelper dbHelper;
	private DataSource dataSource;
	
	public MyModel(Activity a) {
		
		dbHelper = new MyDBHelper(a.getApplicationContext());
		
		if (dbHelper.isExist())   
			dataSource = new DataSource(new DBDataSource(dbHelper));
		else  
			dataSource = new DataSource(new GenerateDataSource());
		
		dataList = dataSource.getData();
	}
	
	public void addData() {
		dataList.add(generateString());
		setChanged();
		notifyObservers();
	}
	
	public void deleteData(int pos) {
		dataList.remove(pos);
		setChanged();
		notifyObservers();
	}
	
	public void sortData() {
		Collections.sort(dataList);
		
		setChanged();
		notifyObservers();
	}
	
	public List<String> getData() {
		return dataList;
	}

	public String generateString() {
		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		int length = random.nextInt(80);
		for (int i = 0; i < length; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		return sb.toString();
	}
	
	public void savaData() {
		dbHelper.saveData(dataList);
	}
	
}
