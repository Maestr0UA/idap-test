package com.example.idaptest;

import java.util.Observable;
import java.util.Observer;
import android.app.Activity;
import android.content.Context;
import android.widget.GridView;

public class MyView extends GridView implements Observer {
	

	DataAdapter adapter;
	
	public MyView(Context context, Activity a, MyModel m) {
		super(context);
		adapter = new DataAdapter(a, m.getData());
		setAdapter(adapter);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		adapter.notifyDataSetChanged();
	}

	
	
}
