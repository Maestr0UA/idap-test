package com.example.idaptest;

import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDBHelper extends SQLiteOpenHelper {
	 
	private static final int DB_VERSION = 1;
	static final String dbName="IDAP.db";
	static final String tableName="RandomTable";
	static final String colData="data";
	static final String colID="ID";
	
	public static final String DATATABLE_CREATE = "create table " +tableName+ " ( _id integer primary key, " + colData + " TEXT);";

	private SQLiteDatabase db;
	
	
	public MyDBHelper(Context context) {
		super(context, dbName, null, DB_VERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATATABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
	}
	
	public void saveData(List<String> arr) {
		db = getWritableDatabase();
		db.delete (MyDBHelper.tableName, null, null);
		ContentValues values = new ContentValues();
		for(int i = 0; i < arr.size(); i++) {
		    values.put(MyDBHelper.colData, arr.get(i));
			db.insert(MyDBHelper.tableName, null, values);
		}
		db.close();
	} 
	
	public boolean isExist() {
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.query(MyDBHelper.tableName, null, null, null, null, null, null);
		if (c.moveToFirst()) {
			return true;
		} else
			return false;
	}
	
}