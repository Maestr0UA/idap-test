package com.example.idaptest;

import java.util.List;

public class DataSource implements dataCreate{
	dataCreate iData;

	public DataSource (dataCreate d) {
		this.iData = d;
	}
	
	@Override
	public List<String> getData() {
		
		return iData.getData();
	}

}
