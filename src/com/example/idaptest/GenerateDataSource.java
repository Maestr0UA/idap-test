package com.example.idaptest;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GenerateDataSource implements dataCreate{

	public String generateString() {
		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		int length = random.nextInt(80);
		for (int i = 0; i < length; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		return sb.toString();
	}

	@Override
	public List<String> getData() {
		List<String> data = new ArrayList<String>();
		for (int i= 0; i < 20; i ++) {
			data.add(generateString());
		}
		return data;
	}
}
